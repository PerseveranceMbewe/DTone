import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';;
import { AppService } from './app.service';
import { ProductsModule } from './components/products/products.module';
import { SharedModule } from './components/shared/shared.module';
import { BenefitsModule } from './components/benefits/benefits.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from  '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { UiModule } from './components/shared/ui/ui.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { CartModule } from './components/cart/cart.module';
import { ToastrModule } from 'ngx-toastr';
import { TransactionsModule } from './components/transactions/transactions.module';
import { NavComponent } from './components/nav/nav.component';



@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    NavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    ProductsModule,
    BenefitsModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    UiModule,
    CommonModule,
    TransactionsModule,
    CartModule,
    ToastrModule.forRoot(),


  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
