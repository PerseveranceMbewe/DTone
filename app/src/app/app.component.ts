import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'et-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'DTone';
  constructor(public appService : AppService){}

}
