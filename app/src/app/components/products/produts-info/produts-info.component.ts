import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EMPTY } from 'rxjs';
import { IProducts } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';
import { ProductsService } from '../products.service';

@Component({
  selector: 'et-produts-info',
  templateUrl: './produts-info.component.html',
  styleUrls: ['./produts-info.component.scss'],
})
export class ProdutsInfoComponent implements OnInit {
  product!: IProducts;
  isLoading: boolean = false;
  productId!: string;
  @Output() productAdded = new EventEmitter();
  constructor(private toastService: ToastrService, private appService: AppService, private route: ActivatedRoute, private router : Router) { }
  ngOnInit(): void {
    this.isLoading = true;
    this.route.paramMap.subscribe((params) => {
      if (params) {
        this.productId = params.get('id') as string;
        this.appService.getProductsById(this.productId).subscribe((product) => {
          this.isLoading = false;
          this.product = product;
        });
      }
    });
  }

  addProductToCart(product: IProducts) {
    this.toastService.info('Product added to cart!');
    this.router.navigate(['/products']);
    this.appService.addProducts(product);
  }
}
