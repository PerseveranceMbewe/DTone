import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  ICountries,
  IOperators,
  IProducts,
  IService,
} from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: IProducts[] = [];
  countries: ICountries[] = [];
  isLoading: boolean = true;
  filterProduct: IProducts[] = [];
  operators: IOperators[] = [];
  searchControl: string = '';
  servicesControl: FormControl = new FormControl();
  countriesFormControl: FormControl = new FormControl();
  operatorsFormControl: FormControl = new FormControl();
  page!: number;
  /* brute forces since not provided from api - ideally api should send this value */
  total_items = 191;
  services: IService[] = [];

  constructor(private service: AppService) { }

  pageChange(page: number) {
    this.page = page;
    this.isLoading = true;
    this.service.getProducts(page).subscribe((pageProducts) => {
      this.products = pageProducts;
      this.isLoading = false;
    });
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.fetchProducts(this.page);
    this.fetchOperationCountries();
    this.handleCountrySelection(this.countriesFormControl);
    this.fetchOperators();
    this.handleOperarSelections(this.operatorsFormControl);
    this.fetchServices();
    this.handleServiceSelected(this.servicesControl);
  }
  fetchOperators() {
    this.service
      .getOperators()
      .subscribe((operator) => (this.operators = operator));
  }

  fetchProducts(page: number) {
    this.service.getProducts(page).subscribe(
      (products) => {
        this.isLoading = false;
        this.products = products;
        this.filterProduct = this.products;
      },
      (error) => {
        this.isLoading = false;
        console.error(error);
      }
    );
  }

  fetchOperationCountries() {
    this.service.getOperationCountries().subscribe(
      (countries) => (this.countries = countries),
      (error) => console.error(error)
    );
  }

  fetchServices() {
    this.service
      .getServices()
      .subscribe((service) => (this.services = service));
  }

  handleCountrySelection(countrySelection: FormControl) {
    countrySelection.valueChanges.subscribe((selectedCountry) => {
      if (!selectedCountry) {
        this.products = this.filterProduct;
      }
       else{
        this.products = this.filterProduct.filter(
          (product) => product.operator.country.name === selectedCountry
        );
       }
    });
  }

  handleServiceSelected(serviceControl: FormControl) {
    serviceControl.valueChanges.subscribe((selectedService) => {
      if (!selectedService) this.products = this.filterProduct;
      else {
        this.products = this.filterProduct.filter(
          (product) => product.service.name === selectedService
        );
      }

    });
  }

  handleOperarSelections(operatorSelection: FormControl) {
    operatorSelection.valueChanges.subscribe(
      (operator) => {
        if (!operator) {
          this.products = this.filterProduct;
        }
        else{
           this.products = this.filterProduct.filter(
            (product) => product.operator.name === operator.name
          );
        }
      }
    );
  }

  resetFilters() {
    this.isLoading = true;
    this.page = 1;
    this.fetchProducts(this.page);
  }
}
