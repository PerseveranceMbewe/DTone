import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';;
import { RouterModule } from '@angular/router';
import { ProductsRoutingModule } from './products-routing.module';
import { ProdutsInfoComponent } from './produts-info/produts-info.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ProductsService } from './products.service';
import { NgSelectModule } from '@ng-select/ng-select';






@NgModule({
  declarations: [
    ProductsComponent,
    ProdutsInfoComponent,
    ProductListComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NgSelectModule,
    FormsModule
  ],
  providers : [ProductsService]
})
export class ProductsModule { }
