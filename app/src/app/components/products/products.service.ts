import { Injectable } from '@angular/core';
import { IProducts } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  cartProductList: IProducts[] = [];
  constructor(private appService: AppService) { }



  addProducts(product: IProducts){
    this.cartProductList.push(product);
  }


  clearCartProducts(){
    this.cartProductList = [];
    return this.cartProductList;
  }

  getCartProducts() {
    return this.cartProductList;
  }
}
