import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';;
import { BenefitsRoutingModule } from './benefits-routing.module';
import { RouterModule } from '@angular/router';
import { BenefitsComponent } from './benefits.component';
import { BenefitsInfoComponent } from './benefits-info/benefits-info.component';


@NgModule({
  declarations: [
    BenefitsComponent,
    BenefitsInfoComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    BenefitsRoutingModule,
  ]
})
export class BenefitsModule { }
