import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BenefitsInfoComponent } from './benefits-info/benefits-info.component';
import { BenefitsComponent } from './benefits.component';

const routes: Routes = [
  {
    path: '',
    component: BenefitsComponent
  },
  {
    path : ':id',
    component : BenefitsInfoComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BenefitsRoutingModule { }
