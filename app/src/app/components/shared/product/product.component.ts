import { Output, EventEmitter } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProducts } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {

  @Input()
  product!: IProducts;
  @Output() emitProduct = new EventEmitter<IProducts>();
  constructor(private router: Router, private appService: AppService, private toastService: ToastrService) { }


  emitPurchase(product: IProducts) {
    this.toastService.info(`added ${this.product.name} to cart`);
    this.appService.addProducts(product);
  }

  emitShowDetails(productId: string) {
    this.router.navigate(['/', 'products', productId])
  }

}
