import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from "./product/product.component";
import { BenefitComponent } from './benefit/benefit.component';
import { UiModule } from './ui/ui.module';
import { CartItemComponent } from './cart-item/cart-item.component';
import { NavigationComponent } from './navigation/navigation.component';
@NgModule({
  declarations: [
    ProductComponent,
    BenefitComponent,
    CartItemComponent,
    NavigationComponent
  ],
  imports: [
    CommonModule,
    UiModule
  ],
  exports: [ProductComponent, BenefitComponent, CartItemComponent]
})
export class SharedModule { }
