import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IProducts } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {
  @Input() product!: IProducts;
  @Output() productRemoved = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  removeProduct(product: IProducts) {
      this.productRemoved.emit(product);
  }

}
