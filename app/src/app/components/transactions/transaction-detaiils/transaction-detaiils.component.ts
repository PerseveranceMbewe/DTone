import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ITransactionsResponseParams } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'et-transaction-detaiils',
  templateUrl: './transaction-detaiils.component.html',
  styleUrls: ['./transaction-detaiils.component.scss'],
})
export class TransactionDetaiilsComponent implements OnInit {
  transactionId!: string;
  isLoading: boolean = false;
  transaction!: ITransactionsResponseParams;
  constructor(
    private appService: AppService,
    private route: ActivatedRoute,
    private toastService: ToastrService
  ) {
    this.isLoading = true;
    this.route.paramMap.subscribe((params) => {
      if (params) {
        this.transactionId = params.get('id') as string;
        this.appService
          .getTransactionById(this.transactionId)
          .subscribe((transaction) => {
            this.isLoading = false;
            this.transaction = transaction;
            console.log('this is the transactions', transaction);
          });
      }
    });
  }

  ngOnInit(): void {}
  downaloadTransaction(transaction: any) {
    this.toastService.success('Donwloading...');
  }
}
