import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionDetaiilsComponent } from './transaction-detaiils/transaction-detaiils.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
const routes: Routes = [{ path: '', component: TransactionListComponent }, {
  path: ':id',
  component: TransactionDetaiilsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
