import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IBalances, ITransactionsResponseParams } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'et-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss'],
})
export class TransactionListComponent implements OnInit {
  transactionlist: ITransactionsResponseParams[] = [];
  isLoading: boolean = false;
  page: number = 1;
  total_items!: number;
  balance: number = 0;
  onHoldBalance: number = 0;
  creditLimit: number = 0;
  searchControl!: string;
  transactionTemp: ITransactionsResponseParams[] = [];
  transactionStatuses = [
    { status: 'Completed' },
    { status: 'Created' },
    { status: 'Cancelled' },
    { status: 'Reversed' },
  ];
  transactionStatusControl: FormControl = new FormControl();
  constructor(private appService: AppService, private toaster: ToastrService, private router: Router) {}

  ngOnInit(): void {
    this.getransactions();
    this.getBalance();

    this.transactionStatusControl.valueChanges.subscribe(
      (transactionStatus) => {
        if (!transactionStatus) {
          this.transactionlist = this.transactionTemp;
          this.total_items = this.transactionlist.length;
        } else {
          this.transactionlist = this.transactionTemp.filter((transaction) => {
            return (
              transaction.status?.message?.toLowerCase() ===
              transactionStatus.status.toLowerCase()
            );
          });
          // filter is empty
          if(this.transactionlist.length  === 0) {
              this.total_items = 0;
          }
          this.total_items = this.transactionlist.length;

        }
      },
      (eror) => console.log(eror)
    );
  }
  pageChange(page: any) {
    this.page = page;
  }

  cancelTransaction(transactionId: number) {
    this.isLoading = true;
    this.appService
      .cancelTransaction(transactionId.toString(), {
        transaction_id: transactionId.toString(),
      })
      .subscribe(
        (response) => {
          if (response) {
            this.toaster.success(
              `transaction ${transactionId} cancelled successfully`
            );
            this.isLoading = false;
            this.getransactions();
            this.getBalance();
          }
        },
        (erorr) => {
          if (erorr) {
            this.toaster.error(
              'Sorry Something went wrong, transaction not cancelled'
            );
            this.isLoading = false;
          }
        }
      );
  }

  getTransactionsDetails(transactionId: string){
    this.router.navigate(['/','transactions',transactionId]);
  }

  resetFilters(){
      this.transactionlist = this.transactionTemp;
      this.transactionStatusControl.reset();
  }

  private getBalance() {
    this.appService.getBalances().subscribe((balance) => {
      if (balance) {
        const balances = balance[0];
        this.balance = balances.available;
        this.onHoldBalance = balances.holding;
        this.creditLimit = balances.credit_limit;
      }
    });
  }

  private getransactions() {
    this.appService.getTransactisonList().subscribe((transactions) => {
      this.transactionTemp = transactions;
      this.transactionlist = transactions.sort((a, b) => {
        return Date.parse(b.creation_date) - Date.parse(a.creation_date);
      });
      this.total_items = this.transactionlist.length;
    });
  }
}
