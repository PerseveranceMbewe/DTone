import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';
import {FormsModule} from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TransactionDetaiilsComponent } from './transaction-detaiils/transaction-detaiils.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    TransactionsComponent,
    TransactionListComponent,
    TransactionDetaiilsComponent
  ],
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    RouterModule
  ]
})
export class TransactionsModule { }
