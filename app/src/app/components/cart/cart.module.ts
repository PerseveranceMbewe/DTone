import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartRoutingModule } from './cart-routing.module';
import { CartListComponent } from './cart-list/cart-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';;
import { CartComponent } from './cart.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CartComponent,
    CartListComponent,
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
  ]
})
export class CartModule { }
