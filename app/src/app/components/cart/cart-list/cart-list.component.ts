import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IProducts } from 'src/app/app-interface';
import { AppService } from 'src/app/app.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss'],
})
export class CartListComponent implements OnInit {
  products = this.appService.getCartProducts();
  isLoading: boolean = false;

  /** used for simiiiulation purposes ideally we would the users phone number**/
  phoneNumbers = [
    {
      id: 0,
      number: '+6595123100',
    },
    {
      id: 1,
      number: '+6595123201',
    },
    {
      id: 2,
      number: '+6595123103',
    },
    {
      id: 3,
      number: '+6595123102',
    },
    {
      id: 4,
      number: '+6595123205',
    },
    {
      id: 5,
      number: '+6595123105',
    },
    {
      id: 6,
      number: '+6595123206',
    },
    {
      id: 6,
      number: '+6595123107',
    },
  ];

  checkoutForm = this.formBuilder.group({
    mobileNumber: ['', [Validators.required]],
    accountNumber: ['', [Validators.required]],
  });

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private toaster: ToastrService,
    private router: Router
  ) {}
  ngOnInit(): void {}

  removeProduct(product: IProducts) {
    this.appService.removeCartProduct(product);
    this.products = this.appService.getCartProducts();
  }

  onSubmit() {
    if (this.checkoutForm.invalid && this.checkoutForm.errors) return;

    if (this.checkoutForm.valid) {
      this.isLoading = true;
      // in this case we have the product already
      this.products.forEach((product, index) => {
        // dialog the form and create payload object
        this.checkoutForm.disable;
        let payload = {
          external_id: uuidv4(),
          product_id: product.id,
          credit_party_identifier: {
            mobile_number: this.checkoutForm.get('mobileNumber')?.value.number,
            account_number: this.checkoutForm.get('accountNumber')?.value,
            account_qualifier: uuidv4(),
          },
        };


        // create transaction for this product,
        this.appService.createTransaction(payload).subscribe(
          (res) => {
            this.isLoading = true;
            // we last item to be process
            if (index === this.products.length - 1) {
              // show toaster and navigate the user to the last transactions items
              this.isLoading = false;
              this.toaster.success('Payments Successfull');
              this.checkoutForm.reset();
              this.appService.clearCartProducts();
              // this navigate to the transactions items
              this.router.navigate(['/', 'transactions']);
            }
          },
          (error) => {
            console.error('payment failed', error);
            this.toaster.error('Payment was not successful');
          }
        );
      });
    }
  }
}
