import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  IBalances,
  ICountries,
  IOperators,
  IProducts,
  IPromotions,
  IService,
  ITransactionsResponseParams,
} from './app-interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization:
        'Basic ' +
        btoa(`${environment.config.api_key}:${environment.config.secret_key}`),
    }),
  };
  cartProductList: IProducts[] = [];
  constructor(private http: HttpClient) {}

  getServices(): Observable<IService[]> {
    return this.http.get<IService[]>('/api/services/', this.httpOptions);
  }

  getProducts(page: number = 1): Observable<IProducts[]> {
    return this.http.get<IProducts[]>(
      `/api/products?page=${page}`,
      this.httpOptions
    );
  }

  getOperators(): Observable<IOperators[]> {
    return this.http.get<IOperators[]>('/api/operators/', this.httpOptions);
  }

  getOperatorsById(operatorId: string) {
    return this.http.get(`/api/operators/${operatorId}`);
  }

  /** get products by id */
  getProductsById(productId: string): Observable<IProducts> {
    return this.http.get<IProducts>(
      `/api/products/${productId}`,
      this.httpOptions
    );
  }

  /** get all promotions */
  getPromotions() {
    return this.http.get('/api/promotions/', this.httpOptions);
  }
  getPromotionById(promotionId: string): Observable<IPromotions> {
    return this.http.get<IPromotions>(
      `/api/promotions/${promotionId}`,
      this.httpOptions
    );
  }

  createTransaction(payload: any) {
    return this.http.post(
      `/api/async/transactions/`,
      payload,
      this.httpOptions
    );
  }

  getBalances() : Observable<IBalances> {
    return this.http.get<IBalances>('/api/balances', this.httpOptions);
  }

  getOperationCountries(): Observable<ICountries[]> {
    return this.http.get<ICountries[]>('/api/countries/', this.httpOptions);
  }

  getTransactisonList(): Observable<ITransactionsResponseParams[]> {
    return this.http.get<ITransactionsResponseParams[]>(
      '/api/transactions/',
      this.httpOptions
    );
  }

  getTransactionById(transactionId: string): Observable<ITransactionsResponseParams> {
    return this.http.get<ITransactionsResponseParams>(
      `/api/transactions/${transactionId}`,
      this.httpOptions
    );
  }
  cancelTransaction(
    transactionId: string,
    payload: { transaction_id: string }
  ) {
    return this.http.post(
      `/api/transactions/${transactionId}/cancel/`,
      payload,
      this.httpOptions
    );
  }



  addProducts(product: IProducts) {
    this.cartProductList.push(product);
  }

  clearCartProducts() {
    this.cartProductList = [];
    return this.cartProductList;
  }

  getCartProducts() {
    return this.cartProductList;
  }

  removeCartProduct(product: IProducts) {
    this.cartProductList = this.cartProductList.filter(
      (filterProduct) => product.id != filterProduct.id
    );
  }

  itemsInCart() {
    return this.cartProductList.length;
  }
}
