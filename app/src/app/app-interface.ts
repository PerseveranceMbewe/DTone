/** defines benefits */
export interface IBenefit {
  type: BenefitType;
  unit_type: string;
  unit: string;
  amount: {
    base: number;
    promotion_bonus: number;
    total_excluding_tax: number;
  };
  additional_information: string;
}

/** defines service */
export interface IService {
  name: string;
  id: number;
}

/** defines Regions and the rest  */
export interface IRegion {
  name: string;
  code: string;
}

export interface ICountries {
  name: string;
  iso_code: string;
  region: IRegion;
}

export interface IOperators {
  id: string;
  name: string;
  country: ICountries;
  regions: IRegion;
}

export type BenefitType = 'TALKTIME' | 'DATA' | 'SMS' | 'PAYMENT' | 'CREDITS';
export interface Benefits {
  name: BenefitType;
}
export type IProductType =
  | 'FIXED_VALUE_RECHARGE'
  | 'RANGED_VALUE_RECHARGE'
  | 'FIXED_VALUE_PIN_PURCHASE'
  | 'RANGED_VALUE_PIN_PURCHASE';
export interface IPromotionProductItem {
  id: number;
  name: string;
  description: string;
  type: IProductType;
}
export interface IPromotions {
  id: number;
  title: string;
  description: string;
  terms: string;
  start_date: string;
  end_date: string;
  operator: IOperators;
  Products: Array<IPromotionProductItem>;
}

export interface IProducts {
  id: string;
  name: string;
  description: string;
  service: {
    id: number;
    name: string;
  };
  operator: IOperators;
  regions: IRegion[];
  type: IProductType;
  validity: {
    unit: string;
    quantity: number;
  };
  required_debit_party_identifier_fields: any[];
  required_credit_party_identifier_fields: any[];
  required_sender_fields: any[];
  required_beneficiary_fields: [];
  required_statement_identifier_fields: [];
  availability_zones: [];
  source: {
    unit_type: string;
    unit: string;
    amount: number;
  };
  destination: {
    unit_type: string;
    unit: string;
    amount: number;
  };
  prices: {
    retail: IPrice;
    wholesale: IPrice;
  };
  rates: {
    base: 0;
    wholesale: 0;
    retail: 0;
  };
  benefits: IBenefit[];
  promotions: IPromotions[];
}

export interface ITransactionsParams {
  external_id?: string;
  product_id?: number;
  calculation_mode: string;

  debit_party_identifier?: {
    mobile_number?: string;
    account_number?: string;
    account_qualifier?: string;
  };
  credit_party_identifier?: {
    mobile_number?: string;
    account_number?: string;
    account_qualifier?: string;
  };
  source?: {};
  destination?: {};
  auto_confirm?: false;
  sender?: {};
  beneficiary?: {};
  statement_identifier?: {};
  callback_url?: string;
}

export interface IPrice {
  amount: number;
  fee: 0;
  unit: string;
  unit_type: string;
}

export interface ITransactionsResponseParams {
  id: number;
  external_id?: string;
  creation_date: string;
  confirmation_expiration_date?: Date;
  confirmation_date?: Date;
  status: {
    id: number;
    message?: string;
    class?: {
      id?: number;
      message?: string;
    };
  };
  operator_reference?: string;
  pin?: {
    code?: string;
    serial?: string;
  };
  product: {
    id: number;
    name: string;
    description: string;
    service: {
      id: 1;
      name: string;
    };
    operator: IOperators;
    regions: IRegion[];
  };
  prices: {
    wholesale: IPrice;
    retail: IPrice;
  };
  rates: {
    base: number;
    wholesale: number;
    retail: number;
  };
  benefits: IBenefit[];
  promotions: IPromotions[];
  requested_values: {
    source: IPrice;
    destination: IPrice;
  };
  adjusted_values: {
    source: IPrice;
    destination: IPrice;
  };
  sender?: any;
  beneficiary: any;
  debit_party_identifier: {
    mobile_number: string;
    account_number: string;
    account_qualifier: string;
  };
  credit_party_identifier: {
    mobile_number: string;
    account_number: string;
    account_qualifier: string;
  };
  statement_identifier: {
    reference: string;
    due_date: string;
  };
}

export type IBalances = [
  {
    id: number;
    unit_type: string;
    unit: string;
    available: number;
    holding: number;
    credit_limit: number;
  }
];
