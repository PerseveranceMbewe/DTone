import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';

const routes: Routes = [{
  path: '',
  component: LandingComponent,
},
{
  path: 'products',
  loadChildren: () => import('./components/products/products.module').then(m => m.ProductsModule)
},
{
  path: 'benefits',
  loadChildren: () => import('./components/benefits/benefits.module').then(m => m.BenefitsModule)
} ,
  {
    path : 'cart',
    loadChildren : () => import('./components/cart/cart.module').then(m => m.CartModule)
  },
  { path: 'transactions', loadChildren: () => import('./components/transactions/transactions.module').then(m => m.TransactionsModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
