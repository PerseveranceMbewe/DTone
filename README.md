 # App assessment 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.1. 

install `npm install -g @angular/cli`  the cli from your terminal then open the `app` folder and run `npm install` and when that is done Runnn `ng serve on the terminal`.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. The dist files will then be used to launch the app into production. 


